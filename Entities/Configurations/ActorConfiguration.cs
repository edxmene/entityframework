using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace entityframework.Entities.Configurations
{
    public class ActorConfiguration : IEntityTypeConfiguration<Actor>
    {
        public void Configure(EntityTypeBuilder<Actor> builder)
        {
            builder.Property(a => a.Bithday).HasColumnType("date"); //date is what sqlserver understands.
            builder.Property(a => a.Fortune).HasPrecision(18,2);
        }
    }
}
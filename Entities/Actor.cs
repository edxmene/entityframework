
namespace entityframework.Entities
{
    public class Actor
    {
        public int Id { get; set; }
        public string Name { get; set; } = string.Empty;
        public decimal Fortune { get; set; }
        public DateTime Bithday { get; set; }
    }
}
namespace entityframework.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; } = string.Empty;
        public bool OnCinema { get; set; }
        public DateTime PremiereDate { get; set; }
    }
}
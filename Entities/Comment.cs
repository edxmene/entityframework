namespace entityframework.Entities
{
    public class Comment
    {
        public int Id { get; set; }
        public string? Content { get; set; }
        public bool Recommend { get; set; }
    }
}